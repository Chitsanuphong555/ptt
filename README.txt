A Pen created at CodePen.io. You can find this one at https://codepen.io/baletsa/pen/oHcfr.

 SCSS mixins for animation and color. Delay variable is used to give a cascade effect to the animation.

Using the mixin from my other pen (http://cdpn.io/omaHe) to show that it can be used for vertical bars as well as horizontal.  